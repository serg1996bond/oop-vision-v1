package com.example.dddwtf.domain.entities;

import com.example.dddwtf.domain.entities.client.Client;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import java.util.UUID;

@NoArgsConstructor
@Entity
public class ElectricMeter {
    @Id
    private UUID id;
    private boolean broken = false;
    private boolean busy = false;
    private Integer reading;
    @OneToOne
    private Client owner;
    //у нас счетчик запоминает последнее запомненое показание, ибо не хочу парится с настройкой линий книги для счетчика и без
    private Integer rememberedReading;

    public Integer getReading() {
        return reading;
    }

    public void imitateWork(Integer addReading) {
        reading += addReading;
    }

    // опять таки лень парится с книгой, потому наш счетчкик ппц как умный
    public Integer getReadingDelta(){
        return reading - rememberedReading;
    }

    public void rememberCurrentReading() {
        this.rememberedReading = reading;
    }

    public void startToUse(){
        this.busy = true;
    }

    public void endToUse(){
        busy = false;
    }
}
