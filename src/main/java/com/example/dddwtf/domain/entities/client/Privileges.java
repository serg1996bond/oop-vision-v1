package com.example.dddwtf.domain.entities.client;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.Embeddable;

@Embeddable
@AllArgsConstructor
@NoArgsConstructor
@Getter
public class Privileges {
    private boolean invalid;
    private boolean pensioner;
}
