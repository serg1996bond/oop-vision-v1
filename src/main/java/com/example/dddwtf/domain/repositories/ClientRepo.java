package com.example.dddwtf.domain.repositories;

import com.example.dddwtf.domain.entities.client.Client;
import org.springframework.data.repository.CrudRepository;

import java.util.UUID;

public interface ClientRepo extends CrudRepository<Client, UUID> {
}
