package com.example.dddwtf.domain.services;

import com.example.dddwtf.domain.entities.client.Client;
import com.example.dddwtf.domain.repositories.ElectricMeterRepo;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
public class ElectricityCostCalculator {

    private final ElectricMeterRepo meterRepo;

    public Integer calculateCostForClient(Client client) {
        Integer coastWithoutDiscount = 0;
        if (client.isUseElectricMeter()) {
            var meter = meterRepo.findByOwner(client)
                    .orElseThrow(RuntimeException::new);
            //чтоб не парится упростим логику
            coastWithoutDiscount = meter.getReadingDelta() * 50;
        } else {
            coastWithoutDiscount = 5000;
        }
        var discount = 0;
        if (client.isInvalid()) discount += coastWithoutDiscount * 0.2;
        if (client.isPensioner()) discount += coastWithoutDiscount * 0.3;
        return coastWithoutDiscount - discount;
    }
}
