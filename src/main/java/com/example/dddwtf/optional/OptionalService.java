package com.example.dddwtf.optional;

import com.example.dddwtf.domain.entities.client.Client;
import com.example.dddwtf.domain.repositories.BookRepo;
import com.example.dddwtf.domain.repositories.ClientRepo;
import com.example.dddwtf.domain.repositories.ElectricMeterRepo;
import com.example.dddwtf.domain.services.ElectricityCostCalculator;
import com.example.dddwtf.dto.ClientInfoDto;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.UUID;

@Service
@AllArgsConstructor
public class OptionalService {

    private final ClientRepo clientRepo;
    private final ElectricMeterRepo electricMeterRepo;
    private final ElectricityCostCalculator electricityCostCalculator;
    private final BookRepo bookRepo;

    //зарегистировать нового клиента
    public void registerNewClient(ClientInfoDto clientInfoDto){
        //создать нового клиента с введеными именем и фамилией а также льготами
        var client = new Client(
                clientInfoDto.getName(),
                clientInfoDto.getPhone(),
                clientInfoDto.getIsInvalid(),
                clientInfoDto.getIsPensioner());
        //сохранить его в базу
        clientRepo.save(client);
    }

    //подключить клиенту счетчик
    private void connectMeterToClient(UUID clientId) {
        //достать из базы клиента
        var client = clientRepo.findById(clientId)
                .orElseThrow(RuntimeException::new);
        //найти в базе рабочий неиспользуемый счетчик
        var electricMeter = electricMeterRepo.findFirstByBrokenIsFalseAndBusyIsFalse()
                .orElseThrow(RuntimeException::new);
        //установить счетчик клиенту
        client.connectElectricMeter(electricMeter);
    }

    //отключить клиенту счетчик
    public void disconnectMeterToClient(UUID clientId) {
        //достаём клиента из бд
        var client = clientRepo.findById(clientId)
                .orElseThrow(RuntimeException::new);
        //отключаем клиенту счетчик
        client.disconnectElectricMeter();
    }

    //выдать клиенту счет за электричество
    public void invoiceClient(UUID clientId) {
        //выбрать клиента
        var client = clientRepo.findById(clientId)
                .orElseThrow(RuntimeException::new);
        //посчитать стоймость использаванной им электроэнергии
        var usedElectricityCost = electricityCostCalculator.calculateCostForClient(client);
        //найти книгу клиента
        var book = bookRepo.findByOwner(client)
                .orElseThrow(RuntimeException::new);
        //оформить счет в книге клиента
        book.addBill(usedElectricityCost);
    }
}
