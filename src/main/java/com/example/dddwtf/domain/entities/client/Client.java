package com.example.dddwtf.domain.entities.client;

import com.example.dddwtf.domain.entities.book.Book;
import com.example.dddwtf.domain.entities.ElectricMeter;
import lombok.NoArgsConstructor;

import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import java.util.UUID;

@NoArgsConstructor
@Entity
public class Client {
    @Id
    private UUID id;
    @Embedded
    private Profile profile;
    @OneToOne
    private ElectricMeter electricMeter;
    @OneToOne
    private Book book;
    @Embedded
    private Privileges privileges;

    public Client(String name, String phone, boolean isInvalid, boolean isPensioner) {
        //АХТУНГ!!! такого быть не должно!!! для такого кода нужны фабрики или что-то еще!!!
        this.profile = new Profile(name, phone);
        this.privileges = new Privileges(isInvalid, isPensioner);
    }

    public boolean isInvalid() {
        return privileges.isInvalid();
    }

    public boolean isPensioner() {
        return privileges.isPensioner();
    }

    public void connectElectricMeter(ElectricMeter electricMeter) {
        //выдаем клиенту счетчик
        this.electricMeter = electricMeter;
        //и запускаем его
        this.electricMeter.startToUse();
    }

    public boolean isUseElectricMeter() {
        return electricMeter != null;
    }

    public void disconnectElectricMeter() {
        //выключаем счетчик
        this.electricMeter.endToUse();
        //и забираем его у пользователя
        this.electricMeter = null;
    }
}
