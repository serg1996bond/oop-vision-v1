package com.example.dddwtf;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DddwtfApplication {

    public static void main(String[] args) {
        SpringApplication.run(DddwtfApplication.class, args);
    }

}
