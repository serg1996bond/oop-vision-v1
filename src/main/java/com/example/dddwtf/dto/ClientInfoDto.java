package com.example.dddwtf.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public class ClientInfoDto {
    private final String name;
    private final String phone;
    private final Boolean isInvalid;
    private final Boolean isPensioner;
}
