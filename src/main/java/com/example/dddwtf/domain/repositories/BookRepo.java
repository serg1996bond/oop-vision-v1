package com.example.dddwtf.domain.repositories;

import com.example.dddwtf.domain.entities.book.Book;
import com.example.dddwtf.domain.entities.client.Client;
import org.springframework.data.repository.CrudRepository;

import java.util.Optional;
import java.util.UUID;

public interface BookRepo extends CrudRepository<Book, UUID> {
    Optional<Book> findByOwner(Client owner);

}
