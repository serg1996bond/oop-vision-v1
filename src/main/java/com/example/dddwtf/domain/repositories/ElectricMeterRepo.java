package com.example.dddwtf.domain.repositories;

import com.example.dddwtf.domain.entities.ElectricMeter;
import com.example.dddwtf.domain.entities.client.Client;
import org.springframework.data.repository.CrudRepository;

import java.util.Optional;
import java.util.UUID;

public interface ElectricMeterRepo extends CrudRepository<ElectricMeter, UUID> {
    Optional<ElectricMeter> findFirstByBrokenIsFalseAndBusyIsFalse();
    Optional<ElectricMeter> findByOwner(Client owner);
}
