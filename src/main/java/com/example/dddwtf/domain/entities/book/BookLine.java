package com.example.dddwtf.domain.entities.book;

import javax.persistence.Embeddable;

@Embeddable
public class BookLine {
    private Integer bill;
    private Integer leftToPay;
}
