package com.example.dddwtf.domain.entities.book;

import com.example.dddwtf.domain.entities.client.Client;
import lombok.NoArgsConstructor;

import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import java.util.List;
import java.util.UUID;

@Entity
@NoArgsConstructor
public class Book {
    @Id
    private UUID id;
    private Integer overpay;
    @ElementCollection
    private List<BookLine> bills;
    @OneToOne
    private Client owner;

    public void addBill(Integer usedElectricityCost) {
        //блабла
    }
}
